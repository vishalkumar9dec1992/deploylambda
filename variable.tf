variable "path_source_code" {
  default = "lambda_function"
}

variable "function_name" {
  default = "parse_my_file"
}

variable "runtime" {
  default = "python3.8"
}

variable "output_path" {
  description = "Path to function's deployment package into local filesystem. eg: /path/lambda_function.zip"
  default = "lambda_function.zip"
}

variable "distribution_pkg_folder" {
  description = "Folder name to create distribution files..."
  default = "lambda_dist_pkg"
}

variable "bucket_for_json_file" {
  description = "Bucket name for put json file to process..."
  default = "vishal-logsbucket"
}
